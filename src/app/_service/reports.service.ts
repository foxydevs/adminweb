import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';

import { environment } from '../../environments/environment'

@Injectable()
export class ReportsService {

  headers = new Headers({
    'Authorization': `Bearer ${localStorage.getItem('token')}`
  })

  constructor(private http:Http) { }

  private handleError(error:any): Promise<any> {
    console.error("Ha ocurrido un prolema")
    return Promise.reject(error.message || error)
  }

  public getEventsByYear(): Promise<any> {
    let url = `${environment.api}/reports/byyearevent?year=2017`
    return this.http.get(url, {headers: this.headers})
                    .toPromise()
                    .then(response => response.json())
                    .catch(this.handleError)
  }

  public getAssistantsvsIntersts(): Promise<any> {
    let url = `${environment.api}/reports/assistantsvsinterests?year=2017`
    return this.http.get(url, {headers: this.headers})
                    .toPromise()
                    .then(response => response.json())
                    .catch(this.handleError)
  }

  public getAdvertisingsActive(): Promise<any> {
    let url = `${environment.api}/reports/advertisingsactive?year=2017`
    return this.http.get(url, {headers: this.headers})
                    .toPromise()
                    .then(response => response.json())
                    .catch(this.handleError)
  }

  public getTotalUsers(): Promise<any> {
    let url = `${environment.api}/reports/totalusers`
    return this.http.get(url, {headers: this.headers})
                    .toPromise()
                    .then(response => response.json())
                    .catch(this.handleError)
  }
}
