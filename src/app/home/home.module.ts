import { NgModule } from '@angular/core';
import { FormsModule } from "@angular/forms";
import { CommonModule } from '@angular/common';
import { DataTableModule } from "angular2-datatable";
import { ChartsModule } from 'ng2-charts';

import {DataFilterPipe} from "./_config/data-filter.pipe";
import { DataFilterAdvertisingsPipe } from './_config/data-filter-advertisings.pipe';

import { HomeRoutingModule } from './home.routing';
import { HomeComponent } from './home.component';
import { UsersComponent } from './users/users.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AdvertisingComponent } from './advertising/advertising.component';
import { UsersService } from '../_service/users.service';
import { AdvertisingService } from '../_service/advertising.service';
import { ReportsService } from '../_service/reports.service';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    HomeRoutingModule,
    DataTableModule,
    ChartsModule
  ],
  providers: [UsersService, AdvertisingService, ReportsService],
  declarations: [HomeComponent, UsersComponent, DashboardComponent, AdvertisingComponent, DataFilterPipe, DataFilterAdvertisingsPipe]
})
export class HomeModule { }
