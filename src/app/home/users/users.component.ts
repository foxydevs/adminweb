import { Component, OnInit } from '@angular/core';
import { UsersService } from '../../_service/users.service';
import { Subject } from 'rxjs/Rx';
import { Users } from '../../classes/users';
import { FormBuilder, FormGroup } from '@angular/forms';

declare var $: any;

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
  titleP: String = "Usuarios"
  titleS: String = "Usuario"

  filterQuery = "";

  dAdmin: number = 1
  dOS: string = "-"
  data:Array<Users> = []
  dataSelect: Users

  constructor(private userService: UsersService) { }

  ngOnInit() {
    this.getUsers()
  }

  getUsers() {

    this.userService.getUsers().then(users => {
      this.data = users
    }).catch(error => {
      console.error(error)
    })
  }

  create(form: any) {
    this.userService.createUser(form.value).then( user => {
      $("#modalCreate").modal("hide")
      $("#formCreate")[0].reset()
      this.getUsers();
    }).catch(error => {
      console.error(error)
    })
  }

  showEdit(user: Users) {
    this.dataSelect = user;
    $("#modalEdit").modal("show")
  }

  updateData(form: any) {
    this.userService.updateUser(form.value, this.dataSelect.id).then(user => {
      $("#formEdit")[0].reset()
      $("#modalEdit").modal("hide")
      this.dataSelect = null
      this.getUsers()
    }).catch(error => {
      console.error(error)
    })
  }

  showDelete(user: Users) {
    this.dataSelect = user;
    $("#modalDelete").modal("show")
  }

  deleteData() {
    this.userService.deleteUser(this.dataSelect.id).then( user => {
      $("#modalDelete").modal("hide")
      this.dataSelect = null
      this.getUsers()
    }).catch(error => {
      console.error(error)
    })
  }
}
