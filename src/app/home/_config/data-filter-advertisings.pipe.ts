import * as _ from "lodash";
import {Pipe, PipeTransform} from "@angular/core";

@Pipe({
    name: "dataFilterAdvertisings"
})
export class DataFilterAdvertisingsPipe implements PipeTransform {

    transform(array: any[], query: string): any {
        if (query) {
            return _.filter(array, row => row.client.indexOf(query) > -1);
        }
        return array;
    }
}
