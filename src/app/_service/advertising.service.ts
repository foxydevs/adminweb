import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Advertisings } from '../classes/advertisings';

import { environment } from '../../environments/environment'

@Injectable()
export class AdvertisingService {

  headers = new Headers({
    'Authorization': `Bearer ${localStorage.getItem('token')}`
  })

  constructor(private http:Http) { }

  private handleError(error:any): Promise<any> {
    console.error("Ha ocurrido un prolema")
    return Promise.reject(error.message || error)
  }

  public getAdvertisings(): Promise<Advertisings[]> {
    let url = `${environment.api}/advertising`
    return this.http.get(url, {headers: this.headers})
                    .toPromise()
                    .then(response => response.json() as Advertisings[])
                    .catch(this.handleError)
  }

  public createAdvertisings(data: any): Promise<Advertisings> {
    let url = `${environment.api}/advertising`
    return this.http.post(url, data, {headers: this.headers})
                    .toPromise()
                    .then(response => response.json() as Advertisings)
                    .catch(this.handleError)
  }

  public updateAdvertisings(data: any, advertising: number): Promise<Advertisings> {
    let url = `${environment.api}/advertising/${advertising}`
    return this.http.put(url, data, {headers: this.headers})
                    .toPromise()
                    .then(response => response.json() as Advertisings)
                    .catch(this.handleError)
  }

  public deleteAdvertisings(advertising: number): Promise<Advertisings> {
    let url = `${environment.api}/advertising/${advertising}`
    return this.http.delete(url, {headers: this.headers})
                    .toPromise()
                    .then(response => response.json() as Advertisings)
                    .catch(this.handleError)
  }
}
