import { Component, OnInit } from '@angular/core';
import { ReportsService } from '../../_service/reports.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  public barChartOptions:any = {
    scaleShowVerticalLines: false,
    responsive: true
  };
  public barChartLabels:string[] = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];
  public barChartType:string = 'bar';
  public barChartLegend:boolean = true;

  public barChartData:any[] = [
    {data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], label: 'Eventos'},
    {data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], label: 'Asistentes'}
  ];

  public pieChartLabels:string[] = ['Asistentes', 'Interesados'];
  public pieChartData:number[] = [10, 10];
  public pieChartType:string = 'pie';

  public publicidadActiva = 0
  public totalUsuarios = 0

  constructor(private reportsService: ReportsService) { }

  ngOnInit() {
    this.reportsService.getEventsByYear().then(data => {
      this.barChartData = [
        {data: data.events, label: 'Eventos'},
        {data: data.assistans, label: 'Asistentes'}
      ]
    }).catch(error => {
      console.error(error)
    })

    this.reportsService.getAssistantsvsIntersts().then(data => {
      this.pieChartData = [data.assistans, data.interests]
    }).catch(error => {
      console.error(error)
    })

    this.reportsService.getAdvertisingsActive().then(data => {
      this.publicidadActiva = data.advertisings
    }).catch(error => {
      console.error(error)
    })

    this.reportsService.getTotalUsers().then(data => {
      this.totalUsuarios = data.users
    }).catch(error => {
      console.error(error)
    })
  }

}
