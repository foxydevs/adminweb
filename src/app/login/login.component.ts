import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router'
import { AuthService } from '../_service/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private route: ActivatedRoute, private router: Router, private authService: AuthService) { }

  ngOnInit() {

  }

  login(form:any) {
    this.authService.doLogin(form).then(user => {
      localStorage.setItem('currentUser', user.username)
      localStorage.setItem('currentEmail', user.email)
      localStorage.setItem('currentFirstName', user.firstname)
      localStorage.setItem('currentLastName', user.lastname)
      localStorage.setItem('currentId', user.id.toString())
      localStorage.setItem('token', user.token)
      this.router.navigate(['home'])
    }).catch(error => {
      console.error(error)
    })
  }
}
