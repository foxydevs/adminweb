import { Injectable } from '@angular/core';
import { Http } from '@angular/http'

import 'rxjs/add/operator/toPromise'
import { environment } from '../../environments/environment'

import {Users} from '../classes/users'

@Injectable()
export class AuthService {

  constructor(private http:Http) { }

  private handleError(error:any): Promise<any> {
    console.error("Ha ocurrido un prolema")
    return Promise.reject(error.message || error)
  }

  public doLogin(form:any): Promise<Users> {
    let url = `${environment.api}/login/admin`
    return this.http.post(url, form)
                    .toPromise()
                    .then(response => response.json() as Users)
                    .catch(this.handleError)
  }

}
