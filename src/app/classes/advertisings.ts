export class Advertisings {
  id:number
  place:string
  description:string
  picture:string
  client:string
  begin:string
  end:string
  state:number
  created_at:string
  updated_at:string
}
