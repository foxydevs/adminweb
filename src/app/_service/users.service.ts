import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http'

import 'rxjs/add/operator/toPromise'
import { environment } from '../../environments/environment'

import {Users} from '../classes/users'

@Injectable()
export class UsersService {

  headers = new Headers({
    'Authorization': `Bearer ${localStorage.getItem('token')}`
  })
  constructor(private http:Http) { }

  private handleError(error:any): Promise<any> {
    console.error("Ha ocurrido un prolema")
    return Promise.reject(error.message || error)
  }

  public getUsers(): Promise<Users[]> {
    let url = `${environment.api}/users`
    return this.http.get(url, {headers: this.headers})
                    .toPromise()
                    .then(response => response.json() as Users[])
                    .catch(this.handleError)
  }

  public createUser(data: any): Promise<Users> {
    let url = `${environment.api}/users`
    return this.http.post(url, data, {headers: this.headers})
                    .toPromise()
                    .then(response => response.json() as Users)
                    .catch(this.handleError)
  }

  public updateUser(data:any, user: number): Promise<Users> {
    let url = `${environment.api}/users/${user}`
    return this.http.put(url, data, {headers: this.headers})
                    .toPromise()
                    .then(response => response.json() as Users)
                    .catch(this.handleError)
  }

  public deleteUser(user: number): Promise<Users> {
    let url = `${environment.api}/users/${user}`
    return this.http.delete(url, {headers: this.headers})
                    .toPromise()
                    .then(response => response.json() as Users)
                    .catch(this.handleError)
  }
}
