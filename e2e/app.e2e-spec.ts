import { AdminFancyPage } from './app.po';

describe('admin-fancy App', () => {
  let page: AdminFancyPage;

  beforeEach(() => {
    page = new AdminFancyPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
