import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  fullname: string = `${localStorage.getItem('currentFirstName')} ${localStorage.getItem('currentLastName')}`

  constructor() { }

  ngOnInit() {
  }

  logout() {
    localStorage.removeItem('currentUser');
    localStorage.removeItem('currentEmail');
    localStorage.removeItem('currentFirstName');
    localStorage.removeItem('currentLastName');
    localStorage.removeItem('currentId');
    localStorage.removeItem('token');
    location.reload();
  }
}
