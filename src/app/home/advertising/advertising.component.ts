import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs/Rx';
import { Advertisings } from '../../classes/advertisings';
import { AdvertisingService } from '../../_service/advertising.service';

declare var $: any;

@Component({
  selector: 'app-advertising',
  templateUrl: './advertising.component.html',
  styleUrls: ['./advertising.component.css']
})
export class AdvertisingComponent implements OnInit {
  titleP: String = "Publicidad"
  titleS: String = "Publicidad"
  data:Array<Advertisings> = []
  dataSelect: Advertisings

  filterQuery = "";
  dState: number = 1

  constructor(private advertisingService: AdvertisingService) { }

  ngOnInit() {
    this.getData()

    $('#date-range').datepicker({
      format: 'yyyy-mm-dd',
      toggleActive: true
    });
  }

  getData() {
    this.data.length = 0
    this.advertisingService.getAdvertisings().then(advertisings => {
      this.data = advertisings
    }).catch(error => {
      console.error(error)
    })
  }

  create(form: any) {
    var dataSend = {};
    $("#formCreate").serializeArray().map(function(x){dataSend[x.name] = x.value;});
    console.log(dataSend)
    this.advertisingService.createAdvertisings(dataSend).then( advertising => {
      $("#modalCreate").modal("hide")
      $("#formCreate")[0].reset()
      this.getData();
    }).catch(error => {
      console.error(error)
    })
  }

  showEdit(advertising: Advertisings) {
    this.dataSelect = advertising;
    $("#modalEdit").modal("show")
  }

  updateData(form: any) {
    this.advertisingService.updateAdvertisings(form.value, this.dataSelect.id).then(advertising => {
      $("#formEdit")[0].reset()
      $("#modalEdit").modal("hide")
      this.dataSelect = null
      this.getData()
    }).catch(error => {
      console.error(error)
    })
  }

  showDelete(advertising: Advertisings) {
    this.dataSelect = advertising;
    $("#modalDelete").modal("show")
  }

  deleteData() {
    this.advertisingService.deleteAdvertisings(this.dataSelect.id).then( advertising => {
      $("#modalDelete").modal("hide")
      this.dataSelect = null
      this.getData()
    }).catch(error => {
      console.error(error)
    })
  }
}
